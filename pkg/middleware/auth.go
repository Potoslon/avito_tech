package middleware

import (
	"fmt"
	"net/http"

	"AvitoTask/pkg/token"
)

func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("auth middleware")
		inToken := r.Header.Get("token")
		userStatus, err := token.ValidateToken(inToken)

		if err != nil {
			next.ServeHTTP(w, r)
			return
		}

		ctx := token.ContextWithStatus(r.Context(), userStatus)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
