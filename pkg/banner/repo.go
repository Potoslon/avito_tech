package banner

import (
	"AvitoTask/pkg/token"
	"context"
	"database/sql"
	"fmt"
	"log"
	"sync"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

func MapToSlice[K comparable, V any](m map[K]V) []V {
	s := make([]V, 0, len(m))
	for _, v := range m {
		s = append(s, v)
	}
	return s
}

var UpdateTime = time.Minute * 5
var CacheSize = 10000
var (
	StatusCreated = 1
	StatusUpdated = 2
	StatusDeleted = 3
)

type BannerMemoryRepository struct {
	BannerDB *sql.DB
	Cache    *BannerCache
}

type BannerRepo interface {
	GetByFeatureTag(ctx context.Context, featureID int, tagID int, useLastVersion bool, userStatus int) (StrippedBanner, error)
	GetByFeatureTagAdmin(ctx context.Context, featureID int, tagID int) ([]Banner, error)
	GetByTag(ctx context.Context, tagID int, offset int, limit int) ([]Banner, error)
	GetByFeature(ctx context.Context, featureID int, offset int, limit int) ([]Banner, error)
	CreateBanner(ctx context.Context, banner Banner) (int, error)
	PatchBanner(ctx context.Context, bannerID int, banner Banner) error
	DeleteByID(ctx context.Context, bannerID int) error
	StartTicker(ctx context.Context)
	updateCache(ctx context.Context) error
}

type TagFeature struct {
	Tag     int
	Feature int
}

type BannerCache struct {
	Banners        *LRUCache[TagFeature, StrippedBanner]
	TagFeatureToID map[int][]TagFeature
	mu             sync.RWMutex
}

/********************************************LRU***************************************************/

type node[K comparable, V any] struct {
	key   K
	Value V
	next  *node[K, V]
	prev  *node[K, V]
}

type LRUCache[K comparable, V any] struct {
	capacity int
	curSize  int
	elems    map[K]*node[K, V]
	head     *node[K, V]
	tail     *node[K, V]
	mu       sync.RWMutex
}

func NewCache[K comparable, V any](capacity int) *LRUCache[K, V] {
	res := LRUCache[K, V]{
		capacity: capacity,
		curSize:  0,
		elems:    make(map[K]*node[K, V], capacity),
		head:     nil,
		tail:     nil,
		mu:       sync.RWMutex{},
	}
	return &res
}

func (c *LRUCache[K, V]) Get(key K) (V, error) {
	c.mu.Lock()
	nodePointer, ok := c.elems[key]
	if ok {
		c.moveForward(nodePointer)
		return nodePointer.Value, nil
	}
	c.mu.Unlock()
	var res V
	return res, fmt.Errorf("not found")
}

func (c *LRUCache[K, V]) Put(key K, value V) {
	c.mu.Lock()
	nodePointer, ok := c.elems[key]
	if ok {
		nodePointer.Value = value
		c.elems[key] = nodePointer
		c.moveForward(nodePointer)
		return
	}

	nodePointer.key = key
	nodePointer.Value = value
	nodePointer = &node[K, V]{
		key:  key,
		next: nil,
		prev: c.head,
	}
	if c.curSize < c.capacity {
		c.curSize += 1
	} else {
		c.removeLast()
	}
	nodePointer.prev = c.head
	if c.head != nil {
		c.head.next = nodePointer
	}
	c.head = nodePointer
	if c.curSize == 1 {
		c.tail = nodePointer
	}

	c.elems[key] = nodePointer
	c.mu.Unlock()
}

func (c *LRUCache[K, V]) moveForward(node *node[K, V]) {
	if node == c.head {
		return
	}
	if node == c.tail {
		c.tail = c.tail.next
		c.tail.prev = nil
	}
	if node.prev != nil {
		node.prev.next = node.next
	}
	if node.next != nil {
		node.next.prev = node.prev
	}
	node.next = nil
	c.head.next = node
	node.prev = c.head
	c.head = node
}

func (c *LRUCache[K, V]) removeLast() {
	delete(c.elems, c.tail.key)
	c.tail = c.tail.next
	if c.tail != nil {
		c.tail.prev = nil
	}
}

func (c *LRUCache[K, V]) removeAny(key K) error {
	nodePointer, ok := c.elems[key]
	if !ok {
		return fmt.Errorf("not found")
	}
	if nodePointer.prev != nil {
		nodePointer.prev.next = nodePointer.next
	} else {
		c.tail = nodePointer.next
	}
	if nodePointer.next != nil {
		nodePointer.next.prev = nodePointer.prev
	} else {
		c.head = nodePointer.prev
	}
	c.curSize -= 1
	delete(c.elems, key)

	return nil
}

/*********************************************BannerCache***************************************************/

func NewBanerCache() *BannerCache {
	return &BannerCache{
		Banners:        NewCache[TagFeature, StrippedBanner](CacheSize),
		TagFeatureToID: make(map[int][]TagFeature, CacheSize),
		mu:             sync.RWMutex{},
	}
}

func (bannerCache *BannerCache) GetByFeatureTag(TagID int, FeatureID int) (StrippedBanner, error) {
	tagFeature := TagFeature{
		Tag:     TagID,
		Feature: FeatureID,
	}
	res, err := bannerCache.Banners.Get(tagFeature)
	if err != nil {
		return res, err
	}
	return res, nil
}

func (bannerCache *BannerCache) InsertByFeatureTag(banner StrippedBanner) {
	tagFeature := TagFeature{
		Tag:     banner.TagID,
		Feature: banner.FeatureID,
	}
	bannerCache.Banners.Put(tagFeature, banner)
	bannerCache.mu.Lock()
	bannerCache.TagFeatureToID[banner.BannerID] = append(bannerCache.TagFeatureToID[banner.BannerID], tagFeature)
	bannerCache.mu.Unlock()
}

func (bannerCache *BannerCache) RemoveByID(BannerID int) error {
	bannerCache.mu.Lock()
	ArrID, ok := bannerCache.TagFeatureToID[BannerID]
	if !ok {
		return nil
	}
	for _, key := range ArrID {
		err := bannerCache.Banners.removeAny(key)
		if err != nil {
			return err
		}
	}
	delete(bannerCache.TagFeatureToID, BannerID)

	bannerCache.mu.Unlock()
	return nil
}

func (bannerCache *BannerCache) UpdateByID(banner Banner, updateStatus int) error {
	bannerCache.mu.Lock()
	ArrID, ok := bannerCache.TagFeatureToID[banner.ID]
	if !ok {
		return nil
	}
	if updateStatus == StatusCreated {
		bannerCache.mu.Unlock()
		return nil
	}

	for _, key := range ArrID {
		err := bannerCache.Banners.removeAny(key)
		if err != nil {
			return err
		}
	}
	delete(bannerCache.TagFeatureToID, banner.ID)

	if updateStatus == StatusDeleted {
		bannerCache.mu.Unlock()
		return nil
	}

	for tagID := range banner.TagsIDs {
		if !banner.IsActive {
			continue
		}
		buf := StrippedBanner{
			BannerID:  banner.ID,
			TagID:     tagID,
			FeatureID: banner.FeatureID,
			Content:   banner.Content,
		}
		bannerCache.InsertByFeatureTag(buf)
	}

	bannerCache.mu.Unlock()
	return nil
}

/***********************************************Repo******************************************************/

func NewMemoryRepo(db *sql.DB, cache *BannerCache) *BannerMemoryRepository {
	return &BannerMemoryRepository{
		BannerDB: db,
		Cache:    cache,
	}
}

func (repo *BannerMemoryRepository) StartTicker(ctx context.Context) {
	ticker := time.NewTicker(UpdateTime)
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case <-ticker.C:
				err := repo.updateCache(ctx)
				if err != nil {
					log.Fatal()
				}
				repo.CleanDB(ctx)
			}
		}
	}()
}

func (repo *BannerMemoryRepository) CleanDB(ctx context.Context) {
	repo.BannerDB.ExecContext(ctx, "Delete FROM banners b WHERE b.update_status = $1 AND b.updated_at < $2",
		StatusDeleted, time.Now().Add(-1*UpdateTime))
	repo.BannerDB.ExecContext(ctx, "Delete FROM tag_to_banner tb WHERE tb.update_status = $1",
		StatusDeleted)
}

func (repo *BannerMemoryRepository) updateCache(ctx context.Context) error {
	t := time.Now().Add(-1 * UpdateTime)
	rows, err := repo.BannerDB.QueryContext(ctx, `SELECT b.id, b.feature_id, array_agg(tb.tag_id), b.title, b.text, b.url, b.is_active, b.update_status 
													FROM banner b  
													JOIN tag_to_banner tb ON b.id = tb.banner_id 
													WHERE b.updated_at <= $1 
													GROUP BY b.id, b.feature_id, b.title, b.text, b.url, b.is_active, b.update_status`,
		t)
	if err != nil {
		return err
	}

	defer rows.Close()

	var updateStatus int
	for rows.Next() {
		var banner Banner
		err := rows.Scan(&banner.ID, &banner.FeatureID, &banner.TagsIDs, &banner.Content.Title, &banner.Content.Text, &banner.Content.Url, &banner.IsActive, &updateStatus)
		if err != nil {
			return err
		}

		err = repo.Cache.UpdateByID(banner, updateStatus)
		if err != nil {
			return err
		}
	}
	return nil
}

func (repo *BannerMemoryRepository) GetByFeatureTag(ctx context.Context, featureID int, tagID int, useLastVersion bool, userStatus int) (StrippedBanner, error) {
	if !useLastVersion {
		res, err := repo.Cache.GetByFeatureTag(tagID, featureID)
		if err == nil {
			return res, nil
		}
	}

	if userStatus == token.Admin {
		var res StrippedBanner
		row := repo.BannerDB.QueryRowContext(ctx, `SELECT b.title, b.text, b.url, b.is_active 
													FROM banner b 
													JOIN tag_to_banner tb ON b.id = tb.banner_id AND b.update_status != $1 AND tb.update_status != $2, 
													WHERE b.feature_id = $3 AND tb.tag_id = $4`,
			StatusDeleted, StatusDeleted, featureID, tagID)

		err := row.Scan(&res.Content.Title, &res.Content.Text, &res.Content.Url, res.IsActive)
		if err != nil {
			return res, err
		}
		if res.IsActive {
			res.FeatureID = featureID
			res.TagID = tagID
			repo.Cache.InsertByFeatureTag(res)
		}
		return res, nil
	}

	var res StrippedBanner
	row := repo.BannerDB.QueryRowContext(ctx, `SELECT b.title, b.text, b.url 
												FROM banner b 
												JOIN tag_to_banner tb ON b.id = tb.banner_id AND b.update_status  != $1 AND tb.update_status != $2, 
												WHERE b.feature_id = $3 AND tb.tag_id = $4 AND b.is_active = $5`,
												StatusDeleted, StatusDeleted, featureID, tagID, true)
												
	err := row.Scan(&res.Content.Title, &res.Content.Text, &res.Content.Url, res.IsActive)
	if err != nil {
		return res, err
	}
	res.FeatureID = featureID
	res.TagID = tagID
	res.IsActive = true
	repo.Cache.InsertByFeatureTag(res)

	return res, nil
}

func (repo *BannerMemoryRepository) GetByFeatureTagAdmin(ctx context.Context, featureID int, tagID int) ([]Banner, error) {
	var res []Banner
	var buf Banner
	row := repo.BannerDB.QueryRowContext(ctx, `SELECT b.id, b.feature_id, array_agg(tb_2.tag_id), b.title, b.text, b.url, b.is_active, b.created_at, b.updated_at 
												FROM banner b 
												JOIN tag_to_banner tb_1 ON b.id = tb_1.banner_id AND b.feature_id = $1 AND tb_1.tag_id = $2 AND b.update_status  != $3 AND tb_1.update_status != $4 
												JOIN tag_to_banner tb_2 ON b.id = tb_2.banner_id AND tb_2.update_status != $5,
												GROUP BY b.id, b.feature_id, b.title, b.text, b.url, b.is_active, b.created_at, b.updated_at`,
		featureID, tagID, StatusDeleted, StatusDeleted, StatusDeleted)

	err := row.Scan(&buf.ID, &buf.FeatureID, &buf.TagsIDs, &buf.Content.Title, &buf.Content.Text, &buf.Content.Url, &buf.IsActive, &buf.CreatedAt, &buf.UpdatedAt)
	if err != nil {
		return res, err
	}
	res = append(res, buf)
	return res, nil
}

func (repo *BannerMemoryRepository) GetByTag(ctx context.Context, tagID int, offset int, limit int) ([]Banner, error) {
	rows, err := repo.BannerDB.QueryContext(ctx, `SELECT b.id, array_agg(tb_2.tag_id), b.feature_id, b.title, b.text, b.url, b.is_active, b.created_at, b.updated_at 
													FROM banner b  
													JOIN tag_to_banner tb_1 ON b.id = tb_1.banner_id AND tb_1.tag_id = $1 AND b.update_status != $2 AND tb_1.update_status != $3
													JOIN tag_to_banner tb_2 ON b.id = tb_2.banner_id AND tb_2.update_status != $4
													GROUP BY b.id, b.feature_id, b.title, b.text, b.url, b.is_active, b.created_at, b.updated_at;`,
		tagID, StatusDeleted, StatusDeleted, StatusDeleted)
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	res := make([]Banner, 0, 100)
	for rows.Next() {
		var buf Banner
		err := rows.Scan(&buf.ID, &buf.TagsIDs, &buf.FeatureID, &buf.Content.Title, &buf.Content.Text, &buf.Content.Url, &buf.IsActive, &buf.CreatedAt, &buf.UpdatedAt)
		if err != nil {
			return nil, err
		}
		res = append(res, buf)
	}

	if offset > len(res) {
		return nil, nil
	}
	res = res[offset:]
	res = res[:limit]
	return res, nil
}

func (repo *BannerMemoryRepository) GetByFeature(ctx context.Context, featureID int, offset int, limit int) ([]Banner, error) {
	rows, err := repo.BannerDB.QueryContext(ctx, `SELECT b.id, array_agg(tb.tag_id), b.feature_id, b.title, b.text, b.url, b.is_active, b.created_at, b.updated_at 
													FROM banner b 
													JOIN tag_to_banner tb ON b.id = tb.banner_id AND b.update_status != $1 AND tb.update_status != $2
													WHERE b.feature_id = $3 
													GROUP BY b.id, b.feature_id, b.title, b.text, b.url, b.is_active, b.created_at, b.updated_at`,
		StatusDeleted, StatusDeleted, featureID)
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	res := make([]Banner, 0, 100)
	for rows.Next() {
		var buf Banner
		err := rows.Scan(&buf.ID, &buf.TagsIDs, &buf.FeatureID, &buf.Content.Title, &buf.Content.Text, &buf.Content.Url, &buf.IsActive, &buf.CreatedAt, &buf.UpdatedAt)
		if err != nil {
			return nil, err
		}
		res = append(res, buf)
	}

	if offset > len(res) {
		return nil, nil
	}
	res = res[offset:]
	res = res[:limit]
	return res, nil
}

func (repo *BannerMemoryRepository) CreateBanner(ctx context.Context, banner Banner) (int, error) {
	var BannerID int64


	insTagString := `BEGIN;
					INSERT INTO banner (feature_id, title, text, url, is_active, created_at, updated_at, update_status) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id;
					INSERT INTO tag_to_banner (tag_id, banner_id, feature_id, update_status) VALUES ($9, $10, $11, $12)`
	

	
	insArgsMass := make([]interface{}, 0, 40)
	insArgsMass = append(insArgsMass, banner.FeatureID)
	insArgsMass = append(insArgsMass, banner.Content.Title)
	insArgsMass = append(insArgsMass, banner.Content.Text) 
	insArgsMass = append(insArgsMass, banner.Content.Url)
	insArgsMass = append(insArgsMass, banner.IsActive)
	insArgsMass = append(insArgsMass, time.Now())
	insArgsMass = append(insArgsMass, time.Now())
	insArgsMass = append(insArgsMass, StatusCreated)

	i := 13
	for _, tagID := range banner.TagsIDs {
		insTagString = fmt.Sprintf(insTagString, ", ($%d, $%d, $%d, $%d)", i, i+1, i+2, i+3)
		insArgsMass = append(insArgsMass, tagID)
		insArgsMass = append(insArgsMass, banner.ID)
		insArgsMass = append(insArgsMass, banner.FeatureID)
		insArgsMass = append(insArgsMass, StatusCreated)
		i += 4
	}

	err := repo.BannerDB.QueryRowContext(ctx, insTagString, insArgsMass...).Scan(&BannerID)

	return int(BannerID), err
}

func (repo *BannerMemoryRepository) PatchBanner(ctx context.Context, bannerID int, banner Banner) error {
	_, err := repo.BannerDB.ExecContext(ctx, "UPDATE banners b SET b.feature_id = $1, b.title = $2, b.text = $3, b.url = $4, b.is_active = $5, b.updated_at = $6, b.update_status = $7, WHERE id = $8",
		banner.FeatureID, banner.Content.Title, banner.Content.Text, banner.Content.Url, banner.IsActive, time.Now(), StatusUpdated, bannerID)
	if err != nil {
		return err
	}

	_, err = repo.BannerDB.ExecContext(ctx, "UPDATE tag_to_banner tb SET tb.update_status = $1, WHERE tb.banner_id = $2",
		StatusDeleted, bannerID)
	if err != nil {
		return err
	}

	for _, tagID := range banner.TagsIDs {
		_, err = repo.BannerDB.ExecContext(ctx, "INSERT INTO tag_to_banner (`tag_id`, `banner_id`, `update_status`) VALUES ($1, $2, $3);",
			tagID, banner.ID, StatusCreated)
		if err != nil {
			return err
		}
	}
	return nil
}

func (repo *BannerMemoryRepository) DeleteByID(ctx context.Context, bannerID int) error {
	tx, err := repo.BannerDB.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	_, err = tx.ExecContext(ctx, "UPDATE banners b SET  b.updated_at = $1, b.update_status, WHERE id = $2",
		time.Now(), StatusDeleted, bannerID)
	if err != nil {
		return err
	}

	_, err = tx.ExecContext(ctx, "UPDATE tag_to_banner tb SET tb.update_status = $1, WHERE tb.banner_id = $2",
		StatusDeleted, bannerID)
	if err != nil {
		return err
	}
	
	tx.Commit()
	return nil
}
