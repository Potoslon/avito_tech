package banner

import "time"

type Content struct {
	Title string `json:"title"`
	Text  string `json:"text"`
	Url   string `json:"url"`
}

type Banner struct {
	ID           int
	TagsIDs      []int     `json:"tag_ids"`
	FeatureID    int       `json:"feature_id"`
	IsActive     bool      `json:"is_active"`
	Content      Content   `json:"content"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
}

type StrippedBanner struct {
	BannerID  int
	TagID     int
	FeatureID int
	Content   Content
	IsActive  bool
}
