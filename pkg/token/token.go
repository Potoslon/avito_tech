package token

import (
	"context"
	"errors"
	"fmt"
	"strconv"
)

type statusKey string

var (
	NotAuthorised = 0
	SimpleUser    = 1
	Admin         = 2
	tokenSecret   = []byte("**********")
)

var tokenKey statusKey = "TokenContextKey"

var (
	ErrNoAuth = errors.New("no session found")
)

func ValidateToken(inToken string) (int, error) {
	if inToken == "" {
		return 0, fmt.Errorf("no token")
	}
	res, err := strconv.Atoi(inToken)
	if err != nil {
		return 0, err
	}
	return res, nil
}

func StatusFromContext(ctx context.Context) int {
	userStatus, ok := ctx.Value(tokenKey).(int)
	if !ok || userStatus == NotAuthorised {
		return NotAuthorised
	}
	return userStatus
}

func ContextWithStatus(ctx context.Context, userStatus int) context.Context {
	return context.WithValue(ctx, tokenKey, userStatus)
}
