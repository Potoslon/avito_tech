package handlers

import (
	"AvitoTask/pkg/banner"
	"AvitoTask/pkg/token"
	"database/sql"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type BannerHandler struct {
	BannerRepo banner.BannerRepo
}

func marshallAndWrite(w http.ResponseWriter, data any) {
	w.Header().Set("Content-Type", "application/json")
	res, err := json.Marshal(data)
	if err != nil {
		http.Error(w, `{"error": "serialization error"}`, http.StatusInternalServerError)
		return
	}
	_, err = w.Write(res)
	if err != nil {
		http.Error(w, `{"error": "serialization error"}`, http.StatusInternalServerError)
		return
	}
}

func (h *BannerHandler) GetUserBanner(w http.ResponseWriter, r *http.Request) {

	var useLastVersion bool

	userStatus := token.StatusFromContext(r.Context())
	if userStatus == token.NotAuthorised {
		http.Error(w, ``, http.StatusUnauthorized)
		return
	}

	tagIDStr := r.URL.Query().Get("tag_id")
	tagID, err := strconv.Atoi(tagIDStr)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, `{"error": "bad tag id"}`, http.StatusBadRequest)
		return
	}

	featureIDStr := r.URL.Query().Get("feature_id")
	featureID, err := strconv.Atoi(featureIDStr)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, `{"error": "bad feature id"}`, http.StatusBadRequest)
		return
	}

	useLastVersionStr := r.URL.Query().Get("use_last_version")
	if useLastVersionStr == "" {
		useLastVersion = false
	}
	useLastVersion, err = strconv.ParseBool(useLastVersionStr)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, `{"error": "bad version flag"}`, http.StatusBadRequest)
		return
	}

	banner, err := h.BannerRepo.GetByFeatureTag(r.Context(), featureID, tagID, useLastVersion, userStatus)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			http.Error(w, ``, http.StatusNotFound)
		} else {
			w.Header().Set("Content-Type", "application/json")
			http.Error(w, `{"error": "internal server error"}`, http.StatusInternalServerError)
		}
		return
	}

	if !banner.IsActive && userStatus != token.Admin {
		http.Error(w, ``, http.StatusForbidden)
		return
	}

	marshallAndWrite(w, banner.Content)
}

func (h *BannerHandler) GetFilterBanner(w http.ResponseWriter, r *http.Request) {
	var res []banner.Banner
	var err error

	// Проверка авторизации пользователя
	userStatus := token.StatusFromContext(r.Context())
	if userStatus == token.NotAuthorised {
		http.Error(w, ``, http.StatusUnauthorized)
		return
	}

	if userStatus != token.Admin {
		http.Error(w, ``, http.StatusForbidden)
		return
	}

	// Чтение query-параметров
	tagIDStr := r.URL.Query().Get("tag_id")
	tagID, tagErr := strconv.Atoi(tagIDStr)

	featureIDStr := r.URL.Query().Get("feature_id")
	featureID, featureErr := strconv.Atoi(featureIDStr)
	if tagErr != nil && featureErr != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, `{"error": "bad IDs"}`, http.StatusBadRequest)
		return
	}

	limit := 0
	limitStr := r.URL.Query().Get("limit")
	if limitStr != "" {
		limit, err = strconv.Atoi(limitStr)
		if err != nil {
			w.Header().Set("Content-Type", "application/json")
			http.Error(w, `{"error": "bad limit"}`, http.StatusBadRequest)
			return
		}
	}

	offset := 0
	offsetStr := r.URL.Query().Get("offset")
	if offsetStr != "" {
		offset, err = strconv.Atoi(offsetStr)
		if err != nil {
			w.Header().Set("Content-Type", "application/json")
			http.Error(w, `{"error": "bad offset"}`, http.StatusBadRequest)
			return
		}
	}

	// Обращение к базе
	if tagErr == nil && featureErr == nil {
		res, err = h.BannerRepo.GetByFeatureTagAdmin(r.Context(), featureID, tagID)
	} else if tagErr == nil {
		res, err = h.BannerRepo.GetByTag(r.Context(), tagID, offset, limit)
	} else if featureErr == nil {
		res, err = h.BannerRepo.GetByFeature(r.Context(), tagID, offset, limit)
	}

	// Пакуем и отправляем результат
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		if errors.Is(err, sql.ErrNoRows) {
			http.Error(w, `{"error": "not found"}`, http.StatusNotFound)
		} else {
			http.Error(w, `{"error": "internal server error"}`, http.StatusInternalServerError)
		}
		return
	}
	marshallAndWrite(w, res)
}

func (h *BannerHandler) Create(w http.ResponseWriter, r *http.Request) {
	userStatus := token.StatusFromContext(r.Context())
	if userStatus == token.NotAuthorised {
		http.Error(w, `{"error": "not authorised"}`, http.StatusUnauthorized)
		return
	}

	if userStatus != token.Admin {
		http.Error(w, `{"error": "not allowed"}`, http.StatusForbidden)
		return
	}

	body, err := io.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, `{"error": "bad form"}`, http.StatusBadRequest)
		return
	}
	var banner banner.Banner

	err = json.Unmarshal(body, &banner)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, `{"error": "bad form"}`, http.StatusBadRequest)
		return
	}

	ID, err := h.BannerRepo.CreateBanner(r.Context(), banner)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, `{"error": "banner creation error"}`, http.StatusInternalServerError)
		return
	}

	resMap := map[string]int{
		"banner_id": ID,
	}
	res, err := json.Marshal(resMap)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, `{"error": "serialization error"}`, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	w.Write(res)
}

func (h *BannerHandler) Patch(w http.ResponseWriter, r *http.Request) {
	userStatus := token.StatusFromContext(r.Context())
	if userStatus == token.NotAuthorised {
		http.Error(w, ``, http.StatusUnauthorized)
		return
	}

	if userStatus != token.Admin {
		http.Error(w, ``, http.StatusForbidden)
		return
	}

	vars := mux.Vars(r)
	bannerIDStr, ok := vars["id"]
	if !ok {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, `{"error": "bad banner id"}`, http.StatusBadRequest)
		return
	}
	bannerID, err := strconv.Atoi(bannerIDStr)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, `{"error": "bad banner id"}`, http.StatusBadRequest)
		return
	}

	body, err := io.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, `{"error": "bad form"}`, http.StatusBadRequest)
		return
	}
	var banner banner.Banner

	err = json.Unmarshal(body, &banner)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, `{"error": "bad form"}`, http.StatusBadRequest)
		return
	}

	err = h.BannerRepo.PatchBanner(r.Context(), bannerID, banner)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			http.Error(w, ``, http.StatusNotFound)
		}
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, `{"error": "banner patching error"}`, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func (h *BannerHandler) Delete(w http.ResponseWriter, r *http.Request) {

	userStatus := token.StatusFromContext(r.Context())
	if userStatus == token.NotAuthorised {
		http.Error(w, ``, http.StatusUnauthorized)
		return
	}
	if userStatus != token.Admin {
		http.Error(w, ``, http.StatusForbidden)
		return
	}

	vars := mux.Vars(r)
	bannerIDStr, ok := vars["id"]
	if !ok {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, `{"error": "bad banner id"}`, http.StatusBadRequest)
		return
	}
	bannerID, err := strconv.Atoi(bannerIDStr)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, `{"error": "bad banner id"}`, http.StatusBadRequest)
		return
	}

	err = h.BannerRepo.DeleteByID(r.Context(), bannerID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			w.Header().Set("Content-Type", "application/json")
			http.Error(w, `{"error": "not found"}`, http.StatusNotFound)
		} else {
			w.Header().Set("Content-Type", "application/json")
			http.Error(w, `{"error": "internal server error"}`, http.StatusInternalServerError)
		}
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
