DROP TABLE IF EXISTS banner CASCADE;
CREATE TABLE banner (
  id SERIAL PRIMARY KEY,
  feature_id int NOT NULL,
  title varchar(255) NOT NULL,
  text varchar(255) NOT NULL,
  url varchar(255) NOT NULL,
  is_active boolean NOT NULL,
  created_at timestamp NOT NULL,
  updated_at timestamp NOT NULL,
  update_status int NOT NULL,
FOREIGN KEY (update_status) REFERENCES update_status(id),
FOREIGN KEY (feature_id) REFERENCES feature(id)
);
