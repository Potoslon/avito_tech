DROP TABLE IF EXISTS tag_to_banner;
CREATE TABLE tag_to_banner (
  id SERIAL PRIMARY KEY,
  banner_id int NOT NULL,
  tag_id int NOT NULL,
  feature_id int NOT NULL,
  update_status int NOT NULL,
FOREIGN KEY (feature_id) REFERENCES feature ( id ),
FOREIGN KEY ( tag_id ) REFERENCES tag ( id ),
FOREIGN KEY ( banner_id ) REFERENCES  banner ( id ),
FOREIGN KEY ( update_status ) REFERENCES update_status ( id ),
CONSTRAINT UNIQUE (tag_id, banner_id)
);