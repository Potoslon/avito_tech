package main

import (
	"AvitoTask/pkg/banner"
	"AvitoTask/pkg/handlers"
	"context"
	"database/sql"
	"fmt"
	"net/http"

	"AvitoTask/pkg/middleware"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user_bd  = "postgres"
	password = "ZHABA"
	dbname   = "postgres"
)

func main() {
	dsn := fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable",
		host, port, user_bd, password, dbname)

	dbBanner, err := sql.Open("postgres", dsn)
	if err != nil {
		panic(err)
	}

	dbBanner.SetMaxOpenConns(100)

	err = dbBanner.Ping()
	if err != nil {
		panic(err)
	}

	cacheBanner := banner.NewBanerCache()

	bannerRepo := banner.NewMemoryRepo(dbBanner, cacheBanner)

	bannerHandler := handlers.BannerHandler{
		BannerRepo: bannerRepo,
	}

	ctx := context.Background()
	bannerHandler.BannerRepo.StartTicker(ctx)

	router := mux.NewRouter()

	router.HandleFunc("/user_banner", bannerHandler.GetUserBanner).Methods("GET")
	router.HandleFunc("/banner", bannerHandler.GetFilterBanner).Methods("GET")
	router.HandleFunc("/banner", bannerHandler.Create).Methods("POST")
	router.HandleFunc("/banner/{id}", bannerHandler.Patch).Methods("PATCH")
	router.HandleFunc("/banner/{id}", bannerHandler.Delete).Methods("DELETE")

	mux := middleware.Auth(router)
	mux = middleware.Panic(mux)

	addr := ":8080"
	err = http.ListenAndServe(addr, mux)
	if err != nil {
		fmt.Println(err.Error())
		ctx.Done()
		return
	}
}
